# Cortex
Cortex was a Python package extend from [Kaggler](https://github.com/jeongyoonlee/Kaggler) for learning algorithms and utility functions for ETL and data analysis. It is distributed under the version 3 of the GNU General Public License.

**Right now cortex is not at the most recent version of Kaggler, we will continue working on this to catch up changes in Kaggler.**

# Dependencies
* sklearn
* numpy/scipy
* pandas
* kaggler