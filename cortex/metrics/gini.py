import numpy as np

def gini_csharp_port(expected, predicted):
    assert expected.shape[0] == predicted.shape[0], 'unequal number of rows'

    # zip (index, predicted, expected)
    _all = np.dstack((
        np.arange(expected.shape[0]),
        predicted,
        expected))[0]

    # order descending by predicted
    # suborder ascending by index
    sort_order = np.lexsort((_all[:, 0], -_all[:, 1]))
    _all = _all[sort_order]

    total_actual_losses = float(np.sum(expected))

    population_delta = 1.0 / expected.shape[0]
    accumulated_population_percentage_sum = 0
    accumulated_loss_percentage_sum = 0

    gini_sum = 0.0

    for row in _all:
        accumulated_loss_percentage_sum += (row[2] / total_actual_losses)
        accumulated_population_percentage_sum += population_delta
        gini_sum += (accumulated_loss_percentage_sum - \
            accumulated_population_percentage_sum)

    gini = gini_sum / expected.shape[0]
    return gini

def gini_normalized_csharp_port(expected, predicted):
    return gini_csharp_port(expected, predicted) / gini_csharp_port(expected, expected)

def nagative_gini_normalized_csharp_port(expected, predicted):
    return -gini_csharp_port(expected, predicted) / gini_csharp_port(expected, expected)
    
eva_fuc = nagative_gini_normalized_csharp_port
    
if __name__ == '__main__':
    a = np.array([5.1, 3.2, 1.7, 6.2, 8.1])
    b = np.array([3.1, 5.2, 2.7, 5.1, 1.1])
    print('{:.20f}'.format(gini_normalized_csharp_port(a, b)))