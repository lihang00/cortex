from sklearn.metrics import roc_auc_score

def neg_auc(y_true, y_pred):
    return -roc_auc_score(y_true, y_pred)

eva_fuc = neg_auc


if __name__ == '__main__':
    import argparse
    import numpy as np
    from sklearn.datasets import load_svmlight_file
    parser = argparse.ArgumentParser()
    parser.add_argument('--trn', required=True, dest='trn')
    parser.add_argument('--tst', required=True, dest='tst')
    args = parser.parse_args()

    y_pred = np.loadtxt(args.tst)
    _, y_true = load_svmlight_file(args.trn)

    print roc_auc_score(y_true, y_pred)
