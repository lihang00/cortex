from sklearn.preprocessing import OneHotEncoder
import numpy as np
from ..utils.logger import log
from statsmodels.distributions.empirical_distribution import ECDF
from scipy.stats import norm
from scipy import sparse
from sklearn.datasets import dump_svmlight_file
from kaggler.data_io import load_data, save_data
from feature_map import FeatureType, FeatureMap


def normalize_numerical_feature2(trn_feature, tst_feature=[], n=None):
    """Normalize the Pandas column based on cumulative distribution.
    
    Args:
        feature: feature vector to normalize.
        n: number of observations to use for deriving probability distribution
           of the feature.  Observations beyond first n observations will be
           normalized based on the probability distribution found from the
           first n observations. 

    Returns:
        A normalized feature vector.
    """

    trn_feature = np.squeeze(np.asarray(trn_feature))
    if not n:
        n = len(trn_feature)

    ecdf = ECDF(trn_feature[:n])

    trn_norm = norm.ppf(ecdf(trn_feature) * .998 + .001)
    trn_norm.shape=(len(trn_norm),1)
    tst_norm = None

    if len(tst_feature) > 0:
        tst_feature = np.squeeze(np.asarray(tst_feature))
        tst_norm = norm.ppf(ecdf(tst_feature) * .998 + .001)
        tst_norm.shape = (len(tst_norm),1)

    return trn_norm, tst_norm

def log_feature_cnt(X_trn, X_tst):
    log.info('TRN {} features'.format(X_trn.shape[1]))
    log.info('TST {} features'.format(X_tst.shape[1]))
    if X_trn.shape[1] != X_tst.shape[1]:
        log.info('WARNING, feature cnt does not match!!!!')

def get_categorical_stat(trn_feature, tst_feature, trn_label, trn_idx=[],
                         sparse_output=True):
    """Stats on categorical features"""
    
    smooth_up = 1
    smooth_down = 3
    local_stats = get_stat(trn_feature, trn_label, smooth_up, smooth_down, trn_idx)
    trn_cat_stats = trn_feature.apply(lambda x: get_pr(local_stats, x, smooth_up, smooth_down))

    if len(trn_idx) > 0:
        trn_stats = get_stat(trn_feature, trn_label, smooth_up, smooth_down)
    else:
        trn_stats = local_stats
    tst_cat_stats = tst_feature.apply(lambda x: get_pr(trn_stats, x, smooth_up, smooth_down))

    if sparse_output:
        return sparse.csr_matrix(np.matrix(trn_cat_stats).reshape(len(trn_feature), 1)), \
               sparse.csr_matrix(np.matrix(tst_cat_stats).reshape(len(tst_feature), 1))
    else:
        return trn_cat_stats, tst_cat_stats


def get_categorical_count_enc(trn_feature, tst_feature, sparse_output=True):
    """Count on categorical features"""

    value_cnt = get_label_count(trn_feature)
    trn_cat_cnt = trn_feature.apply(lambda x: value_cnt.get(x, 0))
    tst_cat_cnt = tst_feature.apply(lambda x: value_cnt.get(x, 0))

    if sparse_output:
        return sparse.csr_matrix(np.matrix(trn_cat_cnt).reshape(len(trn_feature), 1)), \
               sparse.csr_matrix(np.matrix(tst_cat_cnt).reshape(len(tst_feature), 1))
    else:
        return trn_cat_cnt, tst_cat_cnt


def get_label_count(feature):
    label_count = {}
    for label in feature:
        if label in label_count:
            label_count[label] += 1
        else:
            label_count[label] = 1

    return label_count


def get_pr(stats, cat, smooth_up, smooth_down):
    pos_total = stats.get(cat, [smooth_up, smooth_down])
    return pos_total[0] / float(pos_total[1])
    
def get_stat(features, labels, smooth_up, smooth_down, idx=[]):
    stats = {}

    if len(idx)==0:
        idx=range(len(features))

    for i in idx:
        key = features[i]
        label = labels[i]
        if key not in stats:
            stats[key] = [smooth_up, smooth_down] # count of true, total
        stats[key][1] = stats[key][1] + 1
        if label == True or label == 1:
            stats[key][0] = stats[key][0] + 1
    return stats


def get_categorical_label_enc(trn_feature, tst_feature=[], min_obs=10, n=None, 
                              sparse_output=True):
    """Encode the column w label encoding."""

    if not n:
        n = len(trn_feature)

    label_encoder, label_reverse = get_label_encoder(trn_feature[:n], min_obs)
    trn_labels = trn_feature.apply(lambda x: label_encoder.get(x, 0))
    trn_labels = np.matrix(trn_labels).reshape(len(trn_labels), 1)
    
    tst_labels = None
    if len(tst_feature) > 0:
        tst_labels = tst_feature.apply(lambda x: label_encoder.get(x, 0))
        tst_labels = np.matrix(tst_labels).reshape(len(tst_labels), 1)

    if sparse_output:
        return sparse.csr_matrix(trn_labels), \
               sparse.csr_matrix(tst_labels)
    else:
        return trn_labels, tst_labels

def encode_categorical_feature(trn_feature, tst_feature=[], min_obs=10, n=None, 
                               feature_map=None, col=None):
    """Encode the Pandas column into sparse matrix with one-hot-encoding."""

    trn_labels, tst_labels = get_label_encoder(trn_feature, tst_feature, min_obs, n, sparse_output=False)

    enc = OneHotEncoder()
    trn_encode = enc.fit_transform(trn_labels)

    tst_encode = None
    if len(tst_feature) > 0:
        tst_encode = enc.transform(tst_labels)

    if trn_encode.shape[1] == 2:
        trn_encode = trn_encode[:,0]
        if len(tst_feature) > 0:
            tst_encode = tst_encode[:,0]
        if feature_map is not None:
            feature_map.add_feature(col, FeatureType.i)
    elif feature_map is not None:
        labels = np.unique(np.asarray(trn_labels).reshape(-1))
        feature_map.add_features([ '{}-{}'.format(col, label_reverse.get(x, 'Others')) for x in labels], FeatureType.q)

    return trn_encode, tst_encode

def get_label_encoder(feature, min_obs=10):
    label_count = {}
    for label in feature:
        if label in label_count:
            label_count[label] += 1
        else:
            label_count[label] = 1

    label_encoder = {}    
    label_reverse = {}
    for label in label_count.keys():
        if label_count[label] >= min_obs:
            label_encoder[label] = len(label_encoder) + 1
            label_reverse[len(label_encoder)] = label

    return label_encoder, label_reverse

def fillna(df, categorical_columns, numerical_columns, cal_value='EMPTY', num_value=0):
    """Fill Na in dataframe. Categorical col will be filled 'EMPTY'. Numerical col will be filled 0."""
    for col in numerical_columns:
        if col in df.columns:
            df[col].fillna(num_value, inplace=True)
    for col in categorical_columns:
        if col in df.columns:
            df[col].fillna(cal_value, inplace=True)


def detect_column_type(df, exclude_columns, max_distinct_value=20):
    """ Based on data type of columns, detect categorical columns and numerical_columns. """

    categorical_columns = []
    numerical_columns = []

    for col in df.columns:
        if col in exclude_columns:
            continue

        if df[col].dtype == 'object':
            categorical_columns.append(col)
            continue

        log.info('stats feature {}'.format(col))
        unique_value = len(df[col].unique())

        log.info('unique value {}'.format(unique_value))

        if unique_value == 1: # ignore one value col
            continue

        numerical_columns.append(col)
        if unique_value <= max_distinct_value:
            categorical_columns.append(col)

    return categorical_columns, numerical_columns


def append_column(base_matrix, col):
    result = None
    if base_matrix is None:
        result = col
    else:
        result = sparse.hstack((base_matrix, col))
    return result

def dump_feature_files(trn, tst, train_feature_file, test_feature_file, 
                       num_cols, category_cols, target_col, 
                       one_hot_min_obs=50, local_validation=False, 
                       label_encoding=False,
                       one_hot_encoding=False,
                       cat_stats_encoding=False,
                       count_encoding=False, 
                       save_tst_target=False,
                       sparse_output=True,
                       fmap_file=None):
    """ Dump feature df to files """
    fmap = FeatureMap()

    sparse_output = sparse_output or one_hot_encoding
    output_features = list(num_cols)
    fmap.add_features(num_cols, FeatureType.q)

    y_trn = np.array(trn[target_col])
    y_tst = np.zeros(len(tst))

    if save_tst_target:
        y_tst = np.array(tst[target_col])

    if label_encoding:
        for col in category_cols:
            log.info('label encoding feature {}'.format(col))
            trn_col, tst_col = get_categorical_label_enc(trn[col],
                                                          tst[col],
                                                          min_obs=one_hot_min_obs,
                                                          sparse_output=False)
            col_name = '{}-label-enc'.format(col)
            trn[col_name] = trn_col
            tst[col_name] = tst_col
            fmap.add_feature(col_name, FeatureType.c)
            output_features.append(col_name)

    if count_encoding:
        for col in category_cols:
            log.info('count enc feature {}'.format(col))

            trn_col, tst_col = get_categorical_count_enc(trn[col],
                                                         tst[col],
                                                         sparse_output=False)
            
            col_name = '{}-cnt-enc'.format(col)
            trn[col_name] = trn_col
            tst[col_name] = tst_col
            fmap.add_feature(col_name, FeatureType.q)
            output_features.append(col_name)

    if cat_stats_encoding:
        for col in category_cols:
            log.info('stats feature {}'.format(col))

            trn_col, tst_col = get_categorical_stat(trn[col],
                                                    tst[col],
                                                    y_trn,
                                                    sparse_output=False)
            col_name = '{}-target-pr'.format(col)
            trn_col[col_name] = trn_col
            tst_col[col_name] = tst_col
            fmap.add_feature(col_name, FeatureType.q)
            output_features.append(col_name)

    # numerical variables and dense features
    X_trn = trn[output_features]
    X_tst = tst[output_features]
    log_feature_cnt(X_trn, X_tst)

    # category variables
    if one_hot_encoding:
        for col in category_cols:
            log.info('transforming feature {}'.format(col))
            
            trn_col, tst_col = encode_categorical_feature(trn[col],
                                                          tst[col],
                                                          min_obs=one_hot_min_obs,
                                                          feature_map=fmap,
                                                          col=col)

            X_trn = append_column(X_trn, trn_col)
            X_tst = append_column(X_tst, tst_col)
            log_feature_cnt(X_trn, X_tst)

    if sparse_output: 
        # save features as sparse matrix files
        X_trn = X_trn.tocsr()
        X_tst = X_tst.tocsr()
    
    log.info('saving training features')
    save_data(X_trn, y_trn, train_feature_file)

    log.info('saving test features')
    save_data(X_tst, y_tst, test_feature_file)

    if fmap_file:
        log.info('saving fmap file')
        fmap.dump(fmap_file)