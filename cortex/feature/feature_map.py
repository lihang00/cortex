from enum import Enum

class FeatureType(Enum):
    def __str__(self):
        return self.name

    i = 1 # i means this feature is binary indicator feature
    q = 2 # q means this feature is a quantitative value, such as age, time, can be missing
    int = 3 # int means this feature is integer value (when int is hinted, the decision boundary will be integer)
    c = 4 # c means this feature is integer from a label encoding from categorical feature

class FeatureMap(object):
    def __init__(self):
        self.features = []

    def add_feature(self, name, feature_type):
        self.features.append((name, feature_type))

    def add_features(self, names, feature_type):
        for name in names:
            self.features.append((name, feature_type))

    def dump(self, feature_map_file):
        with open(feature_map_file, 'w') as f:
            for i, col in enumerate(self.features):
                f.write('{}\t{}\t{}\n'.format(i, col[0], col[1]))

    def clear(self):
        self.features=[]

    def load(self, feature_map_file):
        self.clear()

        with open(feature_map_file, 'r') as f:
            for line in f:
                idx, name, feature_type = line.split('\t')
                self.add_feature(name, FeatureType[feature_type.strip('\n')])

    def get_features(self, feature_type):
        feature_name = []
        feature_index = []

        for i, col in enumerate(self.features):
            if col[1] == feature_type:
                feature_index.append(i)
                feature_name.append(col[0])

        return feature_name, feature_index

