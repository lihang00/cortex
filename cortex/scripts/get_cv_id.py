import argparse
import numpy as np
import pandas as pd
from sklearn import cross_validation, metrics
from sklearn.datasets import dump_svmlight_file,load_svmlight_file

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-data-file', required=True, dest='train_data')
    parser.add_argument('--target', required=True, dest='target')
    parser.add_argument('--cv-id-file', required=True, dest='cv_file')
    parser.add_argument('--n-folds', '-n', required=False, type=int, dest='n', default=5)
    parser.add_argument('--start', '-s', required=False, type=int, dest='s', default=0)
    parser.add_argument('--non-stratified', required=False, action='store_true', dest='non_stratified', default=False)

    args = parser.parse_args()

    df_trn = pd.read_csv(args.train_data)
    y_trn = np.array(df_trn[args.target])

    if args.non_stratified:
        cv = cross_validation.KFold(len(y_trn), n_folds=args.n, shuffle=True, random_state=1)
    else:
        cv = cross_validation.StratifiedKFold(y_trn, n_folds=args.n, shuffle=True, random_state=1)

    cv_id = np.zeros(len(y_trn))
    for i, (i_trn, i_val) in enumerate(cv, start=args.s):
        cv_id[i_val] = i

    np.savetxt(args.cv_file, cv_id, fmt='%d')

if __name__ == '__main__':
    main()