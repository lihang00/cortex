import argparse
import numpy as np
import os
from sklearn.datasets import load_svmlight_file, dump_svmlight_file
from ..utils.const import FIXED_SEED

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_sps', help='input sparse file')
    parser.add_argument('-o', '--output_sps', help='output sparse file')
    args = parser.parse_args()

    X, y = load_svmlight_file(args.input_sps)
    rand_int = range(X.shape[0])
    np.random.seed(seed=FIXED_SEED)
    np.random.shuffle(rand_int)
    X_rand = X[rand_int]
    y_rand = y[rand_int]

    dump_svmlight_file(X_rand, y_rand, args.output_sps)

if __name__ == '__main__':
    main()