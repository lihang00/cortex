import argparse
import sys
import os
import numpy as np
import pandas as pd
import xgboost as xgb
import importlib

from scipy import sparse
from ..utils.logger import log
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from ..feature.feature_processing import encode_categorical_feature, log_feature_cnt

# ### load data in do training
# dtrain = xgb.DMatrix('../data/agaricus.txt.train')
# dtest = xgb.DMatrix('../data/agaricus.txt.test')
# param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
# watchlist  = [(dtest,'eval'), (dtrain,'train')]
# num_round = 3
# bst = xgb.train(param, dtrain, num_round, watchlist)

# print ('start testing predict the leaf indices')
# ### predict all trees
# leafindex = bst.predict(dtest, pred_leaf = True)
# print leafindex.shape

def generate_gbm_features(train_file, train_output,
                          test_file, test_output, 
                          param):
    
    log.info('load train data')
    X_trn, y_trn = load_svmlight_file(train_file)
    
    num_round = 100
    if 'num_round' in param:
        num_round = param['num_round']

    param['nthread'] = -1

    dtrain = xgb.DMatrix(X_trn, y_trn)

    log.info('train model')
    bst = xgb.train(param, dtrain, num_round, [(dtrain,'train')])

    log.info('load test data')
    X_tst, y_tst = load_svmlight_file(test_file)
    dtest = xgb.DMatrix(X_tst)

    log.info('trn / tst predict and get leaf indices')
    leafindex_tst = bst.predict(dtest, pred_leaf = True)
    leafindex_trn = bst.predict(dtrain, pred_leaf = True)
    trn_sample = len(leafindex_trn)

    df = pd.concat([pd.DataFrame(leafindex_trn), pd.DataFrame(leafindex_tst)])

    log.info('encode categorical features')
    df_enc = None

    for col in df.columns:
        log.info('processing column ' + str(col) )
        if df_enc is None:
            df_enc = pd.get_dummies(df[col])
        else:
            df_enc = pd.concat([df_enc, pd.get_dummies(df[col])], axis=1)

    X_enc = np.array(df_enc)

    log.info('saving training features')
    dump_svmlight_file(X_enc[:trn_sample], y_trn, train_output, zero_based=False)

    log.info('saving test features')
    dump_svmlight_file(X_enc[trn_sample:], y_tst, test_output, zero_based=False)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', '-pt', required=True, dest='project_path')
    parser.add_argument('--params', '-p', required=True, dest='params')
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--train-out-file', '-to', required=True, dest='train_output')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--test-out-file', '-vo', required=True, dest='test_output')

    args = parser.parse_args()

    sys.path.insert(0, args.project_path)

    params_config = importlib.import_module(args.params)

    generate_gbm_features(args.train, args.train_output,
                          args.test, args.test_output,
                          params_config.param)

if __name__=="__main__":
    main()