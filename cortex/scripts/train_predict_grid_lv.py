import argparse
import sys
import numpy as np
import pandas as pd
import time
import cPickle as pickle
import importlib
import gc

from ..utils.const import FIXED_SEED
from ..utils.logger import log
from sklearn.datasets import dump_svmlight_file,load_svmlight_file
from sklearn.grid_search import ParameterGrid
from sklearn.base import clone, RegressorMixin, ClassifierMixin

def train_predict_grid_lv(model_config,
                           evaluate_func,
                           local_train_file, local_val_file,
                           train_file, test_file,
                           predict_local_val_file_prefix,
                           predict_test_file_prefix,
                           predict_local_val_file_best,
                           predict_test_file_best,
                           metrics_file,
                           top_n=10):

    log.info("reading in the local training data")
    X_local_trn, y_local_trn = load_svmlight_file(local_train_file)

    log.info("reading in the local validation data")
    X_local_val, y_local_val = load_svmlight_file(local_val_file)

    log.info("convert to dense")
    X_trn = X_trn.todense()
    X_tst = X_tst.todense()

    param_grid = model_config.param_grid

    cnt = 0
    res = []
    for param in list(ParameterGrid(param_grid)):
        cnt += 1
        log.info("trying param #{}".format(cnt))
        performance, yhat_val = train_predict(model_config.base_clf, evaluate_func, 
                                              X_local_trn, y_local_trn, 
                                              X_local_val, y_local_val, 
                                              param, cnt)
        res.append((performance, yhat_val, param))

    sorted_res = sorted(res, key=lambda x: x[0])
    top_models = sorted_res[:top_n]
    
    for performance, yhat_val, param in top_models:
        log.info("writing local validation predictions to file")
        param_key = "_".join([ str(k) + "=" + str(v) for k,v in param.items()])
        log.info(param_key)
        log.info(performance)
        np.savetxt(predict_local_val_file_prefix + "_" + param_key + ".yhat", yhat_val, fmt='%.6f', delimiter=',')        

    log.info("writing best local val pred to file")
    best_perf, best_yhat_val, best_param = top_models[0]
    print best_param
    np.savetxt(predict_local_val_file_best, best_yhat_val, fmt='%.6f', delimiter=',')

    # dump all metrics 
    with open(metrics_file, "w") as m_w:
        for performance, yhat_val, param in sorted_res:
            line = "Param {0} {1}\n".format({ k:[v] for k,v in param.items()}, performance)
            m_w.write(line)

    del X_local_trn
    del X_local_val
    gc.collect()
    
    # train / pred tst
    log.info("reading in the training data")
    X_trn, y_trn = load_svmlight_file(train_file)
    X_trn = X_trn.todense()

    log.info("reading in the testing data")
    X_tst, y_tst = load_svmlight_file(test_file)
    X_tst = X_tst.todense()

    cnt = 0
    for performance, yhat_val, param in top_models:
        cnt += 1
        log.info("training top model #{}".format(cnt))
        performance, yhat_tst = train_predict(model_config.base_clf, evaluate_func, 
                                              X_trn, y_trn, 
                                              X_tst, y_tst, 
                                              param, cnt, False)

        param_key = "_".join([ str(k) + "=" + str(v) for k,v in param.items()])
        np.savetxt(predict_test_file_prefix + "_" + param_key + ".yhat", yhat_tst, fmt='%.6f', delimiter=',')        

        if cnt == 1:
            np.savetxt(predict_test_file_best, yhat_tst, fmt='%.6f', delimiter=',')

def train_predict(base_clf, evaluate_func, 
                  X_trn, y_trn, 
                  X_tst, y_tst,
                  param, idx, cal_perf=True):  
    log.info('Param {}'.format(idx))

    is_classifier = isinstance(base_clf, ClassifierMixin)

    clf = clone(base_clf)
    clf.set_params(**param)
    log.info(clf)

    clf.fit(X_trn, y_trn)
    if is_classifier:
        yhat_tst = clf.predict_proba(X_tst)[:, 1]
    else:
        yhat_tst = clf.predict(X_tst)
    
    performance = -1
    if cal_perf:
        performance = evaluate_func(y_tst, yhat_tst)

    log.info(param)
    log.info('Param {} Performance {}: {}'.format(idx, evaluate_func.__name__, performance))
    return performance, yhat_tst

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', '-pt', required=True, dest='project_path')
    parser.add_argument('--config', '-c', required=True, dest='cfg')
    parser.add_argument('--local-train-file', '-lt', required=True, dest='local_train')
    parser.add_argument('--local-val-file', '-lv', required=True, dest='local_val')
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--predict-local-val-file-prefix', '-pvf', required=True,
                        dest='predict_local_val')
    parser.add_argument('--predict-test-file-prefix', '-ptf', required=True,
                        dest='predict_test')
    parser.add_argument('--predict-local-val-file-best', '-pvb', required=True,
                        dest='predict_local_val_best')
    parser.add_argument('--predict-test-file-best', '-ptb', required=True,
                        dest='predict_test_best')
    parser.add_argument('--metrics-file', '-mb', required=True,
                        dest='metrics_file')
    parser.add_argument('--top-n', '-n', required=False, type=int, dest='top_n', default=10)
    parser.add_argument('--objective', '-o', required=False, dest='objective', default=None)

    args = parser.parse_args()

    start = time.time()
    sys.path.insert(0, args.project_path)

    model_config = importlib.import_module(args.cfg)
    evaluate_func = None
    if args.objective:
        evaluate_config = importlib.import_module(args.objective)
        evaluate_func = evaluate_config.eva_fuc

    if not evaluate_func:
        raise Exception("Missing evaluate_func ... ")

    train_predict_grid_lv(model_config=model_config,
                        evaluate_func=evaluate_func,
                        local_train_file=args.local_train, local_val_file=args.local_val,
                        train_file=args.train, test_file=args.test,
                        predict_local_val_file_prefix=args.predict_local_val,
                        predict_test_file_prefix=args.predict_test,
                        predict_local_val_file_best=args.predict_local_val_best,
                        predict_test_file_best=args.predict_test_best,
                        metrics_file=args.metrics_file,
                        top_n=args.top_n)

    log.info('finished ({:.2f} sec elasped).'.format(time.time() - start))

if __name__=="__main__":
    main()