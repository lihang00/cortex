#!/usr/bin/env python

from __future__ import division
from sklearn.cross_validation import KFold, StratifiedKFold

import argparse
import numpy as np
import pandas as pd
import sys
import os
import time

import xgboost as xgb
import importlib
from ..utils.logger import log
from ..train_predict import helper
from ..utils.const import FIXED_SEED
from sklearn.datasets import dump_svmlight_file,load_svmlight_file

def train_predict(model_config, evaluate_func, 
                  train_file, test_file, 
                  predict_train_file_prefix, predict_test_file_prefix, 
                  predict_train_file, predict_test_file,
                  metrics_file,
                  model_dir,
                  cvid_file, weight_file, n_fold, retrain):

    model_name = os.path.basename(predict_train_file_prefix) + helper.get_param_key(model_config.param)

    # set xgb parameters
    param_list = model_config.param.items()
    if 'n_stop' not in model_config.param:
        model_config.param['n_stop'] = model_config.param['n_est']

    log.info('Loading training data...')
    X_trn, y_trn = load_svmlight_file(train_file)
    log.info("Loading testing data")
    X_tst, _ = load_svmlight_file(test_file)

    xgtst = xgb.DMatrix(X_tst)

    if cvid_file and os.path.isfile(cvid_file):
        cvid = np.loadtxt(cvid_file, dtype=int)
        cv_start = min(cvid)
    else:
        cvid = np.zeros(len(y_trn))
        cv = StratifiedKFold(y_trn, n_folds=n_fold, shuffle=True, random_state=1)
        cv_start = 1
        for i, (i_trn, i_val) in enumerate(cv, start=cv_start):
            cvid[i_val] = i

        assert np.all(cvid!=0)

    if weight_file and os.path.isfile(weight_file):
        w = pd.read_csv(weight_file, index_col=0)['WEIGHT'].values
    else:
        w = np.ones(len(y_trn))

    p_val = np.zeros_like(y_trn, dtype=np.float64)
    p_tst = np.zeros((X_tst.shape[0],))
    for i in range(cv_start, n_fold + cv_start):
        i_trn = np.where(cvid != i)[0]
        i_val = np.where(cvid == i)[0]

        model_file = os.path.join(model_dir, '{}.trn{}.mdl'.format(model_name, i))
        xgtrn = xgb.DMatrix(X_trn[i_trn], weight=w[i_trn], label=y_trn[i_trn])
        xgval = xgb.DMatrix(X_trn[i_val], weight=w[i_val], label=y_trn[i_val])
        if os.path.exists(model_file) and os.path.exists(model_file + '.n_best'):
            log.info('Loading model #{}...'.format(i))
            clf = xgb.Booster(param_list)
            clf.load_model(model_file)
            if i == cv_start:
                with open(model_file + '.n_best') as f:
                    n_best = int(f.readline().strip())
                log.info('best iteration={}'.format(n_best))
        else:
            log.info('Training model #{}...'.format(i))
            watchlist = [(xgtrn, 'train'), (xgval, 'val')]

            if i == cv_start:
                log.info('Training with early stopping')
                clf = xgb.train(param_list, xgtrn, model_config.param['n_est'], watchlist,
                                early_stopping_rounds=model_config.param['n_stop'])
                n_best = clf.best_iteration
                log.info('best iteration={}'.format(n_best))
                with open(model_file + '.n_best', 'w') as f:
                    f.write('{}\n'.format(n_best))
            else:
                clf = xgb.train(param_list, xgtrn, n_best, watchlist)

            log.info('Saving the trained model.')
            clf.save_model(model_file)

        p_val[i_val] = clf.predict(xgval, ntree_limit=n_best)
        p_tst += clf.predict(xgtst, ntree_limit=n_best) / n_fold

        log.info('{} TRN = {:.6f}'.format(evaluate_func.__name__, evaluate_func(y_trn[i_trn], clf.predict(xgtrn, ntree_limit=n_best))))
        log.info('{} VAL = {:.6f}'.format(evaluate_func.__name__, evaluate_func(y_trn[i_val], p_val[i_val])))

    # dump metrics 
    with open(metrics_file, "w") as m_w:
        line = '{} = {:.6f}'.format(evaluate_func.__name__, evaluate_func(y_trn, p_val))
        m_w.write(line)

    log.info(line)

    log.info('Saving predictions...')
    np.savetxt(predict_train_file, p_val, fmt='%.10f')
    if not retrain:
        np.savetxt(predict_test_file, p_tst, fmt='%.10f')
    else:
        log.info('Retrain best model ...')
        xgtrn = xgb.DMatrix(X_trn, weight=w, label=y_trn)
        clf = xgb.train(param_list, xgtrn, n_best)
        p_tst = clf.predict(xgtst, ntree_limit=n_best)
        np.savetxt(predict_test_file, p_tst, fmt='%.10f')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', '-pt', required=True, dest='project_path')
    parser.add_argument('--config', '-c', required=True, dest='cfg')
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-train-file-prefix', '-p', required=True,
                        dest='predict_train')
    parser.add_argument('--predict-test-file-prefix', '-q', required=True,
                        dest='predict_test')
    parser.add_argument('--predict-train-file', required=True,
                        dest='predict_train_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--metrics-file', '-mb', required=True,
                        dest='metrics_file')
    parser.add_argument('--model-dir', required=True, dest='model_dir')
    parser.add_argument('--cross-validation', '-cv', required=False, type=int, dest='cv', default=5)
    parser.add_argument('--cvid-file', required=False, dest='cvid_file', default=None)
    parser.add_argument('--weight-file', required=False, dest='weight_file', default=None)
    parser.add_argument('--objective', '-o', required=False, dest='objective', default=None)
    parser.add_argument('--retrain', required=False, action='store_true', dest='retrain', default=False)

    args = parser.parse_args()
    sys.path.insert(0, args.project_path)

    model_config = importlib.import_module(args.cfg)

    evaluate_func = None
    if args.objective:
        evaluate_config = importlib.import_module(args.objective)
        evaluate_func = evaluate_config.eva_fuc

    if not evaluate_func:
        raise Exception("Missing evaluate_func ... ")

    start = time.time()
    train_predict(model_config=model_config,
                  evaluate_func=evaluate_func,
                  train_file=args.train_file,
                  test_file=args.test_file,
                  predict_train_file_prefix=args.predict_train,
                  predict_test_file_prefix=args.predict_test,  
                  predict_train_file=args.predict_train_file,
                  predict_test_file=args.predict_test_file,
                  metrics_file=args.metrics_file,
                  model_dir = args.model_dir,
                  cvid_file=args.cvid_file,
                  weight_file=args.weight_file,
                  n_fold=args.cv,
                  retrain=args.retrain
                  )
    log.info('finished ({:.2f} min elasped)'.format((time.time() - start) / 60))

if __name__=="__main__":
    main()