__doc__ = """Ensemble model prediction outputs into one training dataset."""

import argparse
import numpy as np
import os
from sklearn.datasets import load_svmlight_file, dump_svmlight_file

def _ensemble_input(model_names, trn_dir, tst_dir, trn_suffix, yht='yht'):
    trn = []
    tst = []
    for model in model_names:
        trn_file = os.path.join(trn_dir, model % (trn_suffix, yht))
        tst_file = os.path.join(tst_dir, model % ('tst', yht))
        print trn_file
        print tst_file
        assert os.path.isfile(trn_file)
        assert os.path.isfile(tst_file)
        trn.append(np.loadtxt(trn_file, delimiter=','))
        tst.append(np.loadtxt(tst_file, delimiter=','))

    trn_dims = trn[0].shape
    for t in trn:
        for dim_x, dim_y in zip(trn_dims, t.shape):
            assert dim_x == dim_y

    tst_dims = tst[0].shape
    for t in tst:
        for dim_x, dim_y in zip(tst_dims, t.shape):
            assert dim_x == dim_y 

    if len(trn_dims) == 1:
        train = np.matrix(trn).transpose()
        test = np.matrix(tst).transpose()
    else:
        train = np.hstack(trn)
        test = np.hstack(tst)

    return train, test


def _get_trn_output(any_trn_file):
    _, y = load_svmlight_file(any_trn_file)
    return y

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model_list', help='a file containing a list of model names, e.g. gbm2_feature1')
    parser.add_argument('-e', '--esb_name', help='esemble model name, e.g. esb')
    parser.add_argument('-t', '--trn_dir', help='dir containing all trn results')
    parser.add_argument('-v', '--tst_dir', help='dir containing all tst results')
    parser.add_argument('-o', '--output_dir', help='dir of output ensemble features')
    parser.add_argument('-y', '--trn_svm', help='a lightsvm file containing trn input and trn target y')
    parser.add_argument('-s', '--trn', default='trn', help='trn or val1')
    parser.add_argument('-a', '--yhat', default='yht', help='yhat or yht')
    args = parser.parse_args()
    
    with open(args.model_list) as f:
        model_names = filter(lambda x: x != '', f.read().split('\n'))
    model_names = [mn.strip('\r') for mn in model_names]

    print 'Ensemble %d model outputs %s' % (len(model_names), str(model_names))
    if len(model_names) <= 0:
        raise("missing models")

    X_trn, X_tst = _ensemble_input(model_names, args.trn_dir, args.tst_dir, args.trn, args.yhat)
    y_trn = _get_trn_output(args.trn_svm)

    esb_trn = os.path.join(args.output_dir, args.esb_name + '.' + args.trn +'.sps')
    dump_svmlight_file(X_trn, y_trn, esb_trn)
    print 'Train file %s' % esb_trn

    esb_tst = os.path.join(args.output_dir, args.esb_name + '.tst.sps')
    dump_svmlight_file(X_tst, np.zeros(X_tst.shape[0]), esb_tst)
    print 'Test file %s' % esb_tst

if __name__ == '__main__':
    main()