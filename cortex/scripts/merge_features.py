import argparse
import numpy as np
import errno, sys
from scipy import sparse
from sklearn.datasets import dump_svmlight_file,load_svmlight_file

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--feature-file-list', '-f', required=True, dest='feature_file_list')
    parser.add_argument('--merged-file', '-m', required=True, dest='merged_file')
    parser.add_argument('--target-file', '-t', required=False, dest='target_file', default=None)

    args = parser.parse_args()

    
    features = []
    for f in args.feature_file_list.split():
        X_trn, y_trn = load_svmlight_file(f)
        features.append((X_trn, y_trn))

    if len(features) == 0:
        sys.exit(errno.EINVAL) 

    merged_features = sparse.hstack([x[0] for x in features])

    print args.feature_file_list
    y = features[0][1]
    if args.target_file:
        y = np.loadtxt(args.target_file)

    dump_svmlight_file(merged_features, y, args.merged_file, zero_based=False)

if __name__ == '__main__':
    main()