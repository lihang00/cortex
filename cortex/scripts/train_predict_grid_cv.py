import argparse
import sys
import os
import numpy as np
import pandas as pd
from sklearn import cross_validation, metrics
import time
import cPickle as pickle
import importlib
import gc

from ..utils.const import FIXED_SEED
from ..utils.logger import log
from sklearn.datasets import dump_svmlight_file,load_svmlight_file
from sklearn.grid_search import ParameterGrid
from sklearn.base import clone, RegressorMixin, ClassifierMixin
from sklearn.preprocessing import StandardScaler

def train_predict_cv(model_config, evaluate_func,
                           train_file, test_file, 
                           predict_train_file_prefix, predict_test_file_prefix, 
                           predict_train_file_best, predict_test_file_best, metrics_file,
                           n_fold=10, top_n=10, normalize=False, retrain=False):

    log.info("reading in the training data")
    X_trn, y_trn = load_svmlight_file(train_file)

    log.info("reading in the test data")
    X_tst, _ = load_svmlight_file(test_file)

    if normalize:
        log.info('Normalizing data')
        scaler = StandardScaler(with_mean=False)
        X_trn = scaler.fit_transform(X_trn)
        X_tst = scaler.transform(X_tst)

    log.info("convert to dense")
    X_trn = X_trn.todense()
    X_tst = X_tst.todense()

    param_grid = model_config.param_grid

    cnt = 0
    res = []
    for param in list(ParameterGrid(param_grid)):
        cnt += 1
        log.info("trying param #{}".format(cnt))
        performance, yhat_trn, yhat_tst = train_predict(model_config.base_clf, evaluate_func, X_trn, y_trn, X_tst, param, n_fold, cnt)
        res.append((performance, yhat_trn, yhat_tst, param))

    sorted_res = sorted(res, key=lambda x: x[0])
    top_models = sorted_res[:top_n]

    for performance, yhat_trn, yhat_tst, param in top_models:
        log.info("writing test predictions to file")
        param_key = "_".join([ str(k) + "=" + str(v) for k,v in param.items()])
        log.info(param_key)
        log.info(performance)
        np.savetxt(predict_train_file_prefix + "_" + param_key + ".yhat", yhat_trn, fmt='%.10f', delimiter=',')
        if not retrain:
            np.savetxt(predict_test_file_prefix + "_" + param_key + ".yhat", yhat_tst, fmt='%.10f', delimiter=',')        

    log.info("writing best pred to file")
    best_perf, best_yhat_trn, best_yhat_tst, best_param = top_models[0]
    print best_param
    np.savetxt(predict_train_file_best, best_yhat_trn, fmt='%.10f', delimiter=',')
    if not retrain:
        np.savetxt(predict_test_file_best, best_yhat_tst, fmt='%.10f', delimiter=',')

    # dump metrics 
    with open(metrics_file, "w") as m_w:
        for performance, yhat_trn, yhat_tst, param in sorted_res:
            line = "Param {0} {1}\n".format({ k:[v] for k,v in param.items()}, performance)
            m_w.write(line)

    gc.collect()

    if retrain:
        cnt = 0
        for performance, yhat_trn, yhat_tst, param in top_models:
            cnt += 1
            log.info("training top model #{}".format(cnt))
            yhat_tst = train_predict_no_cv(model_config.base_clf, X_trn, y_trn, X_tst, param, cnt)

            param_key = "_".join([ str(k) + "=" + str(v) for k,v in param.items()])
            np.savetxt(predict_test_file_prefix + "_" + param_key + ".yhat", yhat_tst, fmt='%.10f', delimiter=',')        

            if cnt == 1:
                np.savetxt(predict_test_file_best, yhat_tst, fmt='%.10f', delimiter=',')

def train_predict_no_cv(base_clf, X_trn, y_trn, X_tst, param, idx):
    log.info('Param {}'.format(idx))

    is_classifier = isinstance(base_clf, ClassifierMixin)

    clf = clone(base_clf)
    clf.set_params(**param)
    log.info(clf)

    clf.fit(X_trn, y_trn)
    if is_classifier:
        yhat_tst = clf.predict_proba(X_tst)[:, 1]
    else:
        yhat_tst = clf.predict(X_tst)

    log.info(param)
    return yhat_tst

def train_predict(base_clf, evaluate_func, X_trn, y_trn, X_tst, param, n_fold, idx):  
    log.info('Param {}'.format(idx))
    base_clf = clone(base_clf)
    base_clf.set_params(**param)
    log.info(base_clf)
    cv = cross_validation.StratifiedKFold(y_trn, n_folds=n_fold, shuffle=True,
                                          random_state=1)

    yhat_tst = np.zeros(X_tst.shape[0])
    yhat_trn = np.zeros(X_trn.shape[0])

    is_classifier = isinstance(base_clf, ClassifierMixin)

    for i, (i_trn, i_val) in enumerate(cv, start=1):
        clf = clone(base_clf)
        log.info('Param {} Training CV #{} '.format(idx, i))
        X_trn_cv = X_trn[i_trn].copy()
        y_trn_cv = y_trn[i_trn].copy()
        clf.fit(X_trn_cv, y_trn_cv)

        if is_classifier:
            yhat_trn[i_val] = np.array(clf.predict_proba(X_trn[i_val].copy())[:, 1])
            yhat_tst += np.array(clf.predict_proba(X_tst)[:, 1]) / n_fold
            yhat_local_trn = np.array(clf.predict_proba(X_trn_cv)[:, 1])
        else:
            yhat_trn[i_val] = np.array(clf.predict(X_trn[i_val].copy()))
            yhat_tst += np.array(clf.predict(X_tst)) / n_fold
            yhat_local_trn = np.array(clf.predict(X_trn_cv))
        
        log.info('{} TRN = {:.6f}  VAL = {:.6f}'.format(evaluate_func.__name__, evaluate_func(y_trn[i_trn], yhat_local_trn), 
                                                                                evaluate_func(y_trn[i_val], yhat_trn[i_val])))
        
    performance = evaluate_func(y_trn, yhat_trn)
    log.info(param)
    log.info('Param {} Performance {}: {}'.format(idx, evaluate_func.__name__, performance))
    return performance, yhat_trn, yhat_tst

def main():
    parser = argparse.ArgumentParser() 
    parser.add_argument('--path', '-pt', required=True, dest='project_path')
    parser.add_argument('--config', '-c', required=True, dest='cfg')
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--predict-train-file-prefix', '-p', required=True,
                        dest='predict_train')
    parser.add_argument('--predict-test-file-prefix', '-q', required=True,
                        dest='predict_test')
    parser.add_argument('--predict-train-file-best', '-pb', required=True,
                        dest='predict_train_best')
    parser.add_argument('--predict-test-file-best', '-qb', required=True,
                        dest='predict_test_best')
    parser.add_argument('--metrics-file', '-mb', required=True,
                        dest='metrics_file')
    parser.add_argument('--cross-validation', '-cv', required=False, type=int, dest='cv', default=10)
    parser.add_argument('--top-n', '-n', required=False, type=int, dest='top_n', default=10)
    parser.add_argument('--objective', '-o', required=False, dest='objective', default=None)
    parser.add_argument('--normalize', required=False, action='store_true', dest='normalize', default=False)
    parser.add_argument('--retrain', required=False, action='store_true', dest='retrain', default=False)

    args = parser.parse_args()

    start = time.time()
    sys.path.insert(0, args.project_path)

    model_config = importlib.import_module(args.cfg)
    evaluate_func = None
    if args.objective:
        evaluate_config = importlib.import_module(args.objective)
        evaluate_func = evaluate_config.eva_fuc

    if not evaluate_func:
        raise Exception("Missing evaluate_func ... ")

    train_predict_cv(model_config=model_config,
                        evaluate_func=evaluate_func,
                        train_file=args.train,
                        test_file=args.test,
                        predict_train_file_prefix=args.predict_train,
                        predict_test_file_prefix=args.predict_test,                        
                        predict_train_file_best=args.predict_train_best,
                        predict_test_file_best=args.predict_test_best,
                        metrics_file=args.metrics_file,
                        n_fold=args.cv,
                        top_n=args.top_n,
                        normalize=args.normalize,
                        retrain=args.retrain)

    log.info('finished ({:.2f} sec elasped).'.format(time.time() - start))

if __name__=="__main__":
    main()