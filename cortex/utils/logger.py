import logging
import logging.config
import os
from pkg_resources import resource_filename

logging.config.fileConfig(os.path.abspath(resource_filename('cortex.res', 'logging.conf')))
log = logging.getLogger('console') 
