from ..utils.logger import log

def get_param_key(param):
    param_key = "_".join([ str(k) + "=" + str(v) for k,v in param.items()])
    log.info(param_key)
    return param_key
