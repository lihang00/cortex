"""setup.py: setuptools control."""

from setuptools import setup, find_packages

try:
    from pypandoc import convert
    read_md = lambda f: convert(f, 'rst')
except ImportError:
    print("warning: pypandoc module not found, could not convert Markdown to RST")
    read_md = lambda f: open(f, 'r').read()

setup(name='cortex',
      packages=find_packages(),
      entry_points = {
      			"console_scripts": ['train_predict_grid_lv = cortex.scripts.train_predict_grid_lv:main',
      								          'train_predict_grid_cv = cortex.scripts.train_predict_grid_cv:main',
                                'ensemble_data = cortex.scripts.ensemble_data:main',
                                'shuffle_file = cortex.scripts.shuffle_file:main',
                                'shuffle_sps = cortex.scripts.shuffle_sps:main',
                                'merge_features = cortex.scripts.merge_features:main',
                                'generate_gbm_features = cortex.scripts.generate_gbm_features:main',
                                'train_predict_xg_cv = cortex.scripts.train_predict_xg_cv:main',
                                'get_cv_id = cortex.scripts.get_cv_id:main'
      								         ]
          },
      package_data={
        "cortex.res": [ "logging.conf" ],
          },
      install_requires=[
        "sklearn",
        "numpy",
        "scipy",
        "pandas",
        "kaggler"
      ],
      version='0.1',
      description='Code for Kaggle Data Science Competitions.',
      long_description=read_md('README.md'),
      url='https://gitlab.com/lihang00/cortex',
      author='Hang Li, Feng Qi',
      author_email='abc@d.com',
      license='LICENSE',
      zip_safe=False)